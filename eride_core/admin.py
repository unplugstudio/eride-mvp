from django.contrib import admin
from django.contrib.admin.templatetags.admin_urls import admin_urlname
from django.core.urlresolvers import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext as _

from .models import Ride, Order, AdditionalCost, Item


########
# Ride #
########

class OrderInlineAdmin(admin.TabularInline):
    model = Order
    fields = ("order_link", "status", "get_item_count", "get_item_total_display")
    readonly_fields = fields
    max_num = 0  # Don't allow adding more instances via inline
    can_delete = False

    def order_link(self, obj):
        url = reverse(admin_urlname(self.opts, 'change'), args=[obj.pk])
        return format_html("<a href='{}' target='_blank'>{}</a>", url, obj)
    order_link.short_description = _("Order")


@admin.register(Ride)
class RideAdmin(admin.ModelAdmin):
    inlines = [OrderInlineAdmin]
    readonly_fields = (
        "status_changed", "created", "modified", "get_weight_info", "get_value_info")
    fieldsets = (
        (None, {
            "fields": (
                ("date", "rider"), ("max_weight", "max_value"),
                ("get_weight_info", "get_value_info"), ("status", "status_changed"),
                "notes",
            ),
        }),
        (None, {
            "fields": (("created", "modified"),),
        }),
        ("Address", {
            "fields": ("address", ("state", "city"), ("zipcode", "phone")),
        }),
    )

    list_display = (
        "date", "rider", "status", "status_changed", "state", "get_weight_info", "get_value_info")
    list_editable = ("status",)
    list_filter = ("status", "state")
    search_fields = ("rider",)
    date_hierarchy = "date"


#########
# Order #
#########

class AdditionalCostInlineAdmin(admin.TabularInline):
    model = AdditionalCost
    extra = 1


class ItemInlineAdmin(admin.TabularInline):
    model = Item
    extra = 1


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = [AdditionalCostInlineAdmin, ItemInlineAdmin]
    readonly_fields = ("status_changed", "created", "modified", "get_fee_display")
    fieldsets = (
        (None, {
            "fields": (
                "purchaser", ("ride", "assigned_to"), ("status", "status_changed"),
                "notes", "get_fee_display"
            ),
        }),
        (None, {
            "fields": (("created", "modified"),),
        }),
    )

    list_display = (
        "purchaser", "status", "status_changed", "assigned_to", "get_fee_display",
        "ride_link", "get_item_count")
    list_editable = ("status", "assigned_to")
    list_filter = ("status", "assigned_to")
    search_fields = ("purchaser", "ride__rider")
    date_hierarchy = "ride__date"

    def ride_link(self, obj):
        if obj.ride is None:
            return ""
        meta = obj.ride._meta
        url = reverse(admin_urlname(meta, 'change'), args=[obj.ride_id])
        return format_html("<a href='{}' target='_blank'>{}</a>", url, obj.ride)
    ride_link.short_description = _("Ride")
    ride_link.admin_order_field = "ride__date"
