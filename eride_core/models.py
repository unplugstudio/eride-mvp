from decimal import Decimal

from django.db import models
from django.db.models import Max, Sum

from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from model_utils import Choices
from model_utils.models import TimeStampedModel, StatusModel

from localflavor.us.models import USStateField, USZipCodeField

from .utils import format_currency

User = get_user_model()


class Ride(StatusModel, TimeStampedModel):
    """
    A record of rider travelling to transport packages.
    """
    STATUS = Choices(
        ("active", _("Active")),
        ("cancelled", _("Cancelled")),
    )
    date = models.DateField(_("Date"))
    rider = models.CharField(_("Rider"), max_length=100)
    max_weight = models.DecimalField(_("Max weight (lb)"), max_digits=8, decimal_places=2)
    max_value = models.DecimalField(_("Max value ($)"), max_digits=8, decimal_places=2)
    notes = models.TextField(_("Notes"), blank=True)

    address = models.TextField(_("Address"))
    state = USStateField(_("State"))
    city = models.CharField(_("City"), max_length=50)
    zipcode = USZipCodeField(_("Zip code"))
    phone = models.CharField(_("Phone"), max_length=50)

    class Meta:
        ordering = ("-date",)

    def __str__(self):
        return "{} ({})".format(self.date, self.rider)

    def get_weight_usage(self):
        items = Item.objects.filter(order__ride=self).aggregate(Sum("weight"))
        return items["weight__sum"] or 0

    def get_weight_usage_relative(self):
        return self.get_weight_usage() / self.max_weight

    def get_weight_info(self):
        if self.pk is None:
            return ""
        return "{:.2f} / {:.2f} ({:.0%})".format(
            self.get_weight_usage(), self.max_weight, self.get_weight_usage_relative())
    get_weight_info.short_description = _("Weight usage (lb)")
    get_weight_info.admin_order_field = "weight"

    def get_value_usage(self):
        items = Item.objects.filter(order__ride=self).aggregate(Sum("price"))
        return items["price__sum"] or 0

    def get_value_usage_relative(self):
        return self.get_value_usage() / self.max_value

    def get_value_info(self):
        if self.pk is None:
            return ""
        return "{:.2f} / {:.2f} ({:.0%})".format(
            self.get_value_usage(), self.max_value, self.get_value_usage_relative())
    get_value_info.short_description = _("Value usage ($)")
    get_value_info.admin_order_field = "value"


class Order(StatusModel, TimeStampedModel):
    """
    An order with some related Items.
    """
    STATUS = Choices(
        ("quoted", _("Quoted")),
        ("confirmed", _("Confirmed")),
        ("received", _("Received by rider")),
        ("transit", _("In transit")),
        ("delivered", _("Delivered")),
        ("cancelled", _("Cancelled")),
    )
    ride = models.ForeignKey(
        Ride, on_delete=models.CASCADE, related_name="orders", verbose_name=_("Ride"),
        blank=True, null=True)
    purchaser = models.CharField(_("Purchaser"), max_length=100)
    assigned_to = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="assigned_orders",
        verbose_name=_("Assigned to"))
    notes = models.TextField(_("Notes"), blank=True)

    class Meta:
        ordering = ("-status_changed",)

    def __str__(self):
        return self.purchaser

    def get_fee(self):
        items = self.items.aggregate(Sum("price"), Sum("weight"), Max("volume"))
        costs = self.costs.aggregate(Sum("cost"))
        items_total = items["price__sum"] or 0
        items_weight = items["weight__sum"] or 0
        items_vol = items["volume__max"] or 0
        extra_cost = costs["cost__sum"] or 0

        return (
            Decimal(0.06) * items_total +
            3 * items_weight +
            4 * items_vol +
            extra_cost
        )

    def get_fee_display(self):
        return format_currency(self.get_fee())
    get_fee_display.short_description = _("Fee")

    def get_item_count(self):
        return self.items.count()
    get_item_count.short_description = _("Items")

    def get_item_total(self):
        items = self.items.aggregate(Sum("price"))
        return items["price__sum"] or 0

    def get_item_total_display(self):
        return format_currency(self.get_item_total())
    get_item_total_display.short_description = _("Item total")


class AdditionalCost(models.Model):
    """
    An arbitrary cost added to an Order.
    """
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="costs")
    concept = models.CharField(_("Concept"), max_length=100)
    cost = models.DecimalField(_("Cost"), max_digits=8, decimal_places=2)

    def __str__(self):
        return self.concept


class Item(models.Model):
    """
    A particular item purchased and added to an order.
    """
    VOLUME_CHOICES = (
        (1, "1"),
        (2, "2"),
        (3, "3"),
        (4, "4"),
        (5, "5"),
        (6, "6"),
    )
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="items")
    name = models.CharField(_("Name"), max_length=100)
    price = models.DecimalField(_("Price"), max_digits=8, decimal_places=2)
    link = models.URLField(_("Link"), max_length=255)
    weight = models.DecimalField(_("Weight (lb)"), max_digits=8, decimal_places=2)
    volume = models.IntegerField(_("Volume"), choices=VOLUME_CHOICES, default=1)

    def __str__(self):
        return self.name
