import locale

locale.setlocale(locale.LC_ALL, '')


def format_currency(value, grouping=True):
    return locale.currency(value, grouping=grouping)
