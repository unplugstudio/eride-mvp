from django.apps import AppConfig


class ErideCoreConfig(AppConfig):
    name = 'eride_core'
    verbose_name = 'e-ride core'
