"""
WSGI config for eride project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

from environ import Env

env = Env()
env.read_env(".env")  # read .env file next to manage.py
settings_path = env("DJANGO_SETTINGS_MODULE")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_path)

application = get_wsgi_application()
