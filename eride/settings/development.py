from .base import *  # noqa
from .base import env

DEBUG = True

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

FABRIC = {
    "DEPLOY_TOOL": "git",
    "NUM_WORKERS": 4,
    "LOCALE": "en_US.UTF-8",  # Should end with ".UTF-8"
    "REQUIREMENTS_PATH": "requirements.txt",
    "HOSTS": env("FABRIC_HOSTS", cast=list),  # List of hosts to deploy to (eg, first host)
    "SSH_USER": env("FABRIC_SSH_USER"),  # SSH username for host deploying to
    "SSH_PASS": env("FABRIC_SSH_PASS"),  # SSH pass for remote server
    "DOMAINS": env("FABRIC_DOMAINS", cast=list),  # Domains for public site
    "DB_PASS": env("FABRIC_DB_PASS"),
    "SECRET_KEY": env("SECRET_KEY"),

    "EMAIL_HOST_USER": env("FABRIC_EMAIL_HOST_USER"),
    "EMAIL_HOST": env("FABRIC_EMAIL_HOST"),
    "EMAIL_HOST_PASSWORD": env("FABRIC_EMAIL_HOST_PASSWORD"),
}
