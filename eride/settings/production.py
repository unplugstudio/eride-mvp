from .base import *  # noqa
from .base import env

DEBUG = False

ALLOWED_HOSTS = ["eride.unplug.studio"]

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTOCOL", "https")

SESSION_COOKIE_SECURE = CSRF_COOKIE_SECURE = True

CACHE_MIDDLEWARE_SECONDS = 60

CACHE_MIDDLEWARE_KEY_PREFIX = env("KEY_PREFIX")

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.memcached.MemcachedCache",
        "LOCATION": "127.0.0.1:11211",
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

# Email settings
EMAIL_HOST_USER = env("EMAIL_HOST_USER")
EMAIL_USE_TLS = True
EMAIL_HOST = env("EMAIL_HOST")
EMAIL_HOST_PASSWORD = env("EMAIL_HOST_PASSWORD")
EMAIL_PORT = 587
SERVER_EMAIL = EMAIL_HOST_USER
# SEND_BROKEN_LINK_EMAILS = True

ADMINS = (
    ("Unplug Studio Developers", "developers@unplug.studio"),
)
MANAGERS = ADMINS
