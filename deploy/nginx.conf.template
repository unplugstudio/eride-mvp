
upstream %(proj_name)s {
    server unix:%(proj_path)s/gunicorn.sock fail_timeout=0;
}

# Main server only listens to HTTPS
server {
    # listen 443 ssl;
    server_name %(domains_nginx)s;
    client_max_body_size 10M;
    keepalive_timeout    15;
    error_log /home/%(user)s/logs/%(proj_name)s_error_nginx.log info;

    # Deny illegal Host headers
    if ($host !~* ^(%(domains_regex)s)$ ) {
        return 444;
    }

    location / {
        proxy_redirect      off;
        proxy_set_header    Host                    $host;
        proxy_set_header    X-Real-IP               $remote_addr;
        proxy_set_header    X-Forwarded-For         $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Protocol    $scheme;
        proxy_pass          http://%(proj_name)s;
    }

    location /static/ {
        root            %(proj_path)s/%(proj_name)s;
        access_log      off;
        log_not_found   off;
        expires 30d;
    }

    location /robots.txt {
        root            %(proj_path)s/static;
        access_log      off;
        log_not_found   off;
    }

    location /favicon.ico {
        root            %(proj_path)s/static/img;
        access_log      off;
        log_not_found   off;
    }


    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/%(domains_nginx)s/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/%(domains_nginx)s/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

server {
    if ($host = %(domains_nginx)s) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    server_name %(domains_nginx)s;
    return 404; # managed by Certbot
}
